// https://jsonplaceholder.typicode.com/todos
// console.log(fetch(`https://jsonplaceholder.typicode.com/todos`))

// solution #3
    // create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API

fetch(`https://jsonplaceholder.typicode.com/todos`)
.then( data => data.json() )
.then( data => {
    data.forEach(element => {console.log(element)})
})

// solution #4 
    // using the data retreived, create an array using the map method to return just the title of every item and print the result in the console
let array= [];
fetch(`https://jsonplaceholder.typicode.com/todos`)
.then( data => data.json() )
.then( data => {
    let titleArray = data.map(element => { return element.title })
    titleArray.forEach(element => {
        // console.log(element)
        array.push(element)
        console.log(array)
    })
})

// solution #5
    // create a fetch request using the GET method that will retreive a single to do list item from JSON Placeholder API

fetch(`https://jsonplaceholder.typicode.com/todos/4`)
.then(response => response.json())
.then(response => {
    console.log(response)
})

// solution #6
    // using the data retreived, print a message in the console that will provide the title and status of the to do list item

fetch(`https://jsonplaceholder.typicode.com/todos/4`)
.then(response => response.json())
.then(response => {
    const {title, completed} = response
    console.log(`The item ${title} on the list has a status of ${completed}`)
})

// solution #7
    // create a fetch request using the POST method that will create a to do list item using JSON Placeholder API

fetch(`https://jsonplaceholder.typicode.com/todos`, {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        completed: true,
        id: 201,
        title: "Created To Do List Item",
        userId: 24
    })
})
.then( response => response.json() )
.then( response => {
    console.log(response)
})

// solution #8 
    // create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API

fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update the to do list with a different data structure",
        id: 1,
        title: "Updated To Do List Item",
        userId: 1
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})

// solution #9
    // update a to do list item by changing the data structure to contain the following properties: title, description, status, date completed, user id

fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "07/09/21",
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})

// solution #10
    // create a fetch request using the PATCH method that will update a to do lit item using the JSON Placeholder API

fetch(`https://jsonplaceholder.typicode.com/todos/24`, {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "03/07/22",
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})

// solution #11
    // update a to do list item by changing the status to complete and add a date when the status was changed

fetch(`https://jsonplaceholder.typicode.com/todos/3`, {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        completed: true,
        status: "completed",
        dateCompleted: "02/04/20"
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})

// solution #12
    // create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API

fetch(`https://jsonplaceholder.typicode.com/todos/2`, {
    method: "DELETE"
})

// solution #13
    // create a request via Postman to retreive all the to do list items
        // GET HTTP method
        // https://jsonplaceholder.typicode.com/todos URI endpoint
        // save this request as get all to do list items

// solution #14
    // create a request via Postman to retreive an individual to do list item
        // GET HTTP method
        // https://jsonplaceholder.typicode.com/todos/1 URI endpoint
        // save this request as get to do list items

// solution #15
    // create a request via Postman to create a to do list item
        // POST HTTP method
        // https://jsonplaceholder.typicode.com/todos/1 URI endpoint
        // save this request as create to do list items

// solution #16
    // create a request via Postman to update a to do list item
        // PUT HTTP method
        // https://jsonplaceholder.typicode.com/todos/1 URI endpoint
        // save this request as create to do list items
        // update the to do list item to mirror the data structure of the PUT fetch request

// solution #17
    // create a request via Postman to update a to do list item
        // PATCH HTTP method
        // https://jsonplaceholder.typicode.com/todos/1 URI endpoint
        // save this request as create to do list items
        // update the to do list item to mirror the data structure of the PATCH fetch request

// solution #18
    // create a request via Postman to delete a to do list item
        // DELETE HTTP method
        // https://jsonplaceholder.typicode.com/todos/1 URI endpoint
        // save this request as delete to do list item
